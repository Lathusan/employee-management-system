import React from 'react'

function Teams() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
                    <div class="card-body">
                    <ul class="list-group list-group-horizontal-lg">
                        <li class="list-group-item text-center active button-5"><a href="/all" class="text-white">All</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/team">Teams</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/office">Offices</a></li>
                    </ul>
                    </div>
                </div>
            <div class="card shadow-sm grow ctm-border-radius">
                <div class="card-body align-center">
                    <h4 class="card-title float-left mb-0 mt-2">6 Teams</h4>
                    <ul class="nav nav-tabs float-right border-0 tab-list-emp">
                        <li class="nav-item pl-3">
                            <a href="javascript:void(0)" class="btn btn-theme button-1 text-white ctm-border-radius p-2 add-person ctm-btn-padding" data-toggle="modal" data-target="#addTeam"><i class="fa fa-plus"></i> Add Team</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="ctm-border-radius shadow-sm grow card">
                        <div class="card-header">
                            <h4 class=" card-title d-inline-block mb-0 mt-2">PHP</h4>
                            <div class="team-action-icon float-right">
                                <span data-toggle="modal" data-target="#edit">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="modal" data-target="#delete">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="avatar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Maria Cotton"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" alt="Maria Cotton" class="img-fluid"/></span>
                            <a href="javascript:void(0)" class="btn btn-theme float-right ctm-border-radius text-white button-1" data-toggle="modal" data-target="#addmembers">Add Members</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ctm-border-radius shadow-sm grow card">
                        <div class="card-header">
                            <h4 class="page-sub-title d-inline-block mb-0 mt-2">Designing</h4>
                            <div class="team-action-icon float-right">
                                <span data-toggle="modal" data-target="#edit">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Edit"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="modal" data-target="#delete">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Delete"><i class="fa fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="avatar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Danny Ward"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" alt="Danny Ward" class="img-fluid"/></span>
                            <a href="javascript:void(0)" class="btn btn-theme float-right ctm-border-radius text-white button-1" data-toggle="modal" data-target="#addmembers">Add Members</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ctm-border-radius shadow-sm grow card">
                        <div class="card-header">
                            <h4 class="page-sub-title d-inline-block mb-0 mt-2">IOS</h4>
                            <div class="team-action-icon float-right">
                                <span data-toggle="modal" data-target="#edit">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Edit"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="modal" data-target="#delete">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Delete"><i class="fa fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="avatar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Danny Ward"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" alt="Danny Ward" class="img-fluid"/></span>
                            <a href="javascript:void(0)" class="btn btn-theme float-right ctm-border-radius text-white button-1" data-toggle="modal" data-target="#addmembers">Add Members</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ctm-border-radius shadow-sm grow card">
                        <div class="card-header">
                            <h4 class="page-sub-title d-inline-block mb-0 mt-2">Android</h4>
                            <div class="team-action-icon float-right">
                                <span data-toggle="modal" data-target="#edit">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Edit"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="modal" data-target="#delete">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Delete"><i class="fa fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="avatar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Danny Ward"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" alt="Danny Ward" class="img-fluid"/></span>
                            <a href="javascript:void(0)" class="btn btn-theme float-right ctm-border-radius text-white button-1" data-toggle="modal" data-target="#addmembers">Add Members</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ctm-border-radius shadow-sm grow card">
                        <div class="card-header">
                            <h4 class="page-sub-title d-inline-block mb-0 mt-2">Business</h4>
                            <div class="team-action-icon float-right">
                                <span data-toggle="modal" data-target="#edit">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Edit"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="modal" data-target="#delete">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Delete"><i class="fa fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="avatar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Danny Ward"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" alt="Danny Ward" class="img-fluid"/></span>
                            <a href="javascript:void(0)" class="btn btn-theme float-right ctm-border-radius text-white button-1" data-toggle="modal" data-target="#addmembers">Add Members</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ctm-border-radius shadow-sm grow card">
                        <div class="card-header">
                            <h4 class="page-sub-title d-inline-block mb-0 mt-2">Testing</h4>
                            <div class="team-action-icon float-right">
                                <span data-toggle="modal" data-target="#edit">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Edit"><i class="fa fa-pencil"></i></a>
                                </span>
                                <span data-toggle="modal" data-target="#delete">
                                <a href="javascript:void(0)" class="btn btn-theme text-white ctm-border-radius" title="Delete"><i class="fa fa-trash"></i></a>
                                </span>
                            </div>
                        </div>
                        <div class="card-body">
                            <span class="avatar" data-toggle="tooltip" data-placement="top" title="" data-original-title="Danny Ward"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" alt="Danny Ward" class="img-fluid"/></span>
                            <a href="javascript:void(0)" class="btn btn-theme button-1 float-right ctm-border-radius text-white" data-toggle="modal" data-target="#addmembers">Add Members</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Teams;