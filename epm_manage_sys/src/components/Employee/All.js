import React from 'react'

function All() {
    return (

    <div class="col-xl-9 col-lg-8 col-md-12">
        <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
            <div class="card-body">
                <ul class="list-group list-group-horizontal-lg">
                    <li class="list-group-item text-center active button-5"><a href="/all" class="text-white">All</a></li>
                    <li class="list-group-item text-center button-6"><a class="text-dark" href="/teams">Teams</a></li>
                    <li class="list-group-item text-center button-6"><a class="text-dark" href="/office">Offices</a></li>
                </ul>
            </div>
        </div>
        <div class="card shadow-sm grow ctm-border-radius">
            <div class="card-body align-center">
                <h4 class="card-title float-left mb-0 mt-2">7 People</h4>
                <ul class="nav nav-tabs float-right border-0 tab-list-emp">
                    <li class="nav-item">
                        <a class="nav-link active border-0 font-23 grid-view" href="/all"><i class="fa fa-th-large" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link border-0 font-23 list-view" href="#"><i class="fa fa-list-ul" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item pl-3">
                        <a href="/addperson" class="btn btn-theme button-1 text-white ctm-border-radius p-2 add-person ctm-btn-padding"><i class="fa fa-plus"></i> Add Person</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="ctm-border-radius shadow-sm grow card">
            <div class="card-body">
        
                <div class="row people-grid-row">
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Maria Cotton</a></h4>
                                            <div>
                                                <p class="mb-0"><b>PHP Team Lead</b></p>
                                                <p class="mb-0 ctm-text-sm">mariacotton@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Danny Ward</a></h4>
                                            <div>
                                                <p class="mb-0"><b>Designing Team Lead</b></p>
                                                <p class="mb-0 ctm-text-sm">dannyward@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-4.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Linda Craver</a></h4>
                                            <div>
                                                <p class="mb-0"><b>IOS Team Lead</b></p>
                                                <p class="mb-0 ctm-text-sm">lindacraver@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-3.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Jenni Sims</a></h4>
                                            <div>
                                                <p class="mb-0"><b>Android Team Lead</b></p>
                                                <p class="mb-0 ctm-text-sm">jennisims@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-2.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">John Gibbs</a></h4>
                                            <div>
                                                <p class="mb-0"><b>	Business Team Lead</b></p>
                                                <p class="mb-0 ctm-text-sm">johndrysdale@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-8.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Stacey Linville</a></h4>
                                            <div>
                                                <p class="mb-0"><b>	Testing Team Lead</b></p>
                                                <p class="mb-0 ctm-text-sm">staceylinville@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile last-card-row">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Richard Wilson</a></h4>
                                            <div>
                                                <p class="mb-0"><b>	Operation Manager</b></p>
                                                <p class="mb-0 ctm-text-sm">richardwilson@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile last-card-row">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-15.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="employment.html" class="text-primary">Daniel Griffing</a></h4>
                                            <div>
                                                <p class="mb-0"><b>	Designing Team</b></p>
                                                <p class="mb-0 ctm-text-sm">danielgriffing@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xl-4">
                        <div class="card widget-profile last-card-row1">
                            <div class="card-body">
                                <div class="pro-widget-content text-center">
                                    <div class="profile-info-widget">
                                        <a href="/employment" class="booking-doc-img">
                                            <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-14.jpg" alt="User Image"/>
                                        </a>
                                        <div class="profile-det-info">
                                            <h4><a href="/employment" class="text-primary">Michelle Fairfax</a></h4>
                                            <div>
                                                <p class="mb-0"><b>PHP Team</b></p>
                                                <p class="mb-0 ctm-text-sm">michellefairfax@example.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
    );
}

export default All;