import React from 'react'

function SideNav() {
    return (
        
        <div class="col-xl-3 col-lg-4 col-md-12 theiaStickySidebar">
        <aside class="sidebar sidebar-user">
           
            <div class="user-card card shadow-sm bg-white text-center ctm-border-radius grow">
                <div class="user-info card-body">
                    <div class="user-avatar mb-4">
                        <img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-9.jpg" alt="User Avatar" class="img-fluid rounded-circle" width="100"/>
                    </div>
                    <div class="user-details">
                        <h4><b>Welcome to Admin</b></h4>
                        <p>Thu, 28 May 2020</p>
                    </div>
                </div>
            </div>
    
            <div class="sidebar-wrapper d-lg-block d-md-none d-none">
                <div class="card ctm-border-radius shadow-sm border-none grow">
                    <div class="card-body">
                        <div class="row no-gutters">
                            
                        <div class="col-6 align-items-center text-center">
                            <a href="/dashboard" class="text-white active p-4 first-slider-btn ctm-border-right ctm-border-left ctm-border-top"><span class="lnr lnr-home pr-0 pb-lg-2 font-23"></span><span class="">Dashboard</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">											
                            <a href="/all" class="text-dark p-4 second-slider-btn ctm-border-right ctm-border-top"><span class="lnr lnr-users pr-0 pb-lg-2 font-23"></span><span class="">Employees</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">												
                            <a href="/company" class="text-dark p-4 ctm-border-right ctm-border-left"><span class="lnr lnr-apartment pr-0 pb-lg-2 font-23"></span><span class="">Company</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">												
                            <a href="/calendar" class="text-dark p-4 ctm-border-right"><span class="lnr lnr-calendar-full pr-0 pb-lg-2 font-23"></span><span class="">Calendar</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">											
                            <a href="/leave" class="text-dark p-4 ctm-border-right ctm-border-left"><span class="lnr lnr-briefcase pr-0 pb-lg-2 font-23"></span><span class="">Leave</span></a>											
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">											
                            <a href="/reviews" class="text-dark p-4 last-slider-btn ctm-border-right"><span class="lnr lnr-star pr-0 pb-lg-2 font-23"></span><span class="">Reviews</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">												
                            <a href="team-reports" class="text-dark p-4 ctm-border-right ctm-border-left"><span class="lnr lnr-rocket pr-0 pb-lg-2 font-23"></span><span class="">Reports</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">												
                            <a href="/account-roles" class="text-dark p-4 ctm-border-right"><span class="lnr lnr-sync pr-0 pb-lg-2 font-23"></span><span class="">Manage</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">											
                            <a href="/general" class="text-dark p-4 last-slider-btn1 ctm-border-right ctm-border-left"><span class="lnr lnr-cog pr-0 pb-lg-2 font-23"></span><span class="">Settings</span></a>												
                        </div>
                        <div class="col-6 align-items-center shadow-none text-center">											
                            <a href="/employment" class="text-dark p-4 last-slider-btn ctm-border-right"><span class="lnr lnr-user pr-0 pb-lg-2 font-23"></span><span class="">Profile</span></a>												
                        </div>

                        </div>
                    </div>
                </div>
            </div>
            
            
        </aside>
    </div>

    );
}

export default SideNav;