import React from 'react'

function AccountRoles() {
    return (

        <div class="col-xl-9 col-lg-8 col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal-lg">
                            <li class="list-group-item text-center active button-5"><a href="/account-roles" class="text-white">Account Roles</a></li>
                            <li class="list-group-item text-center button-6"><a class="text-dark" href="/leadership-roles">Leadership Roles</a></li>
                        </ul>
                    </div>
                </div>
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Super Admin</h4>
                        </div>
                        <div class="card-body">
                            <p class="card-text">They can see and do everything – best not to have many with this role.</p>
                            <div class="mt-2">
                                <span class="avatar" data-toggle="tooltip" data-placement="top" title="Richard Wilson"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg" alt="Richard Wilson" class="img-fluid"/></span>
                                <a href="#" class="btn btn-theme button-1 ctm-border-radius text-white float-right text-white">View Permissions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Admin</h4>
                        </div>
                        <div class="card-body">
                            <p class="card-text">Admin to help sort stuff, but have less access to confidential information like salaries.</p>
                            <div class="mt-2">
                                <span class="avatar" data-toggle="tooltip" data-placement="top" title="Richard Wilson"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg" alt="Richard Wilson" class="img-fluid"/></span>
                                <a href="#" class="btn btn-theme button-1 ctm-border-radius text-white float-right text-white">View Permissions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Payroll Admin</h4>
                        </div>
                        <div class="card-body">
                            <p class="card-text">They sort out your payroll and have access to everyone's salary information.</p>
                            <div class="mt-2">
                                <span class="avatar" data-toggle="tooltip" data-placement="top" title="Richard Wilson"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg" alt="Richard Wilson" class="img-fluid"/></span>
                                <a href="#" class="btn btn-theme button-1 ctm-border-radius text-white float-right text-white">View Permissions</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Team Member</h4>
                        </div>
                        <div class="card-body">
                            <p class="card-text">Team Members have the most limited access – most people should have this role.</p>
                            <div class="mt-2">
                                <span class="avatar" data-toggle="tooltip" data-placement="top" title="Maria Cotton"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" alt="Maria Cotton" class="img-fluid"/></span>
                                <a href="#" class="btn btn-theme button-1 ctm-border-radius text-white float-right text-white">View Permissions</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default AccountRoles;