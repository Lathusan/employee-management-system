import React from 'react'

function TimeOff() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal-lg">
                            <li class="list-group-item text-center button-6"><a href="/general" class="text-dark">General</a></li>
                            <li class="list-group-item text-center active button-5"><a class="text-white" href="/timeoff">Time Off</a></li>
                        </ul>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-6 d-flex">
                    <div class="card flex-fill ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">
                                Company Default <a href="javascript:void(0)" class="float-right text-primary" data-toggle="modal" data-target="#edit_timedefault"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-6 allowance text-center">
                                    <p class="mb-2 btn btn-theme text-white ctm-border-radius">25 days</p>
                                    <p class="mb-2 h6">Allowance</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6 allowance text-center">
                                    <p class="mb-2 btn btn-theme text-white ctm-border-radius">01 January</p>
                                    <p class="mb-2 h6">Year Start</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex">
                    <div class="card flex-fill ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">
                                <span>Working Week</span><a href="javascript:void(0)" class="float-right text-primary" data-toggle="modal" data-target="#addWorkWeek"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            </h4>
                            <span class="ctm-text-sm">Set the dates that your company works.</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <span class="badge custom-badge badge-primary">Mon</span>
                                    <span class="badge custom-badge badge-primary">Tue</span>
                                    <span class="badge custom-badge badge-primary">Wed</span>
                                    <span class="badge custom-badge badge-primary">Thu</span>
                                    <span class="badge custom-badge badge-primary">Fri</span>
                                    <span class="badge custom-badge">Sat</span>
                                    <span class="badge custom-badge">Sun</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Working From Home</h4>
                            <span class="mb-0 ctm-text-sm">Reflect your company's working from home policy by editing the approval process or disabling the feature entirely.</span>
                        </div>
                        <div class="card-body">
                            <div class="card-content">
                                <span class="wk-home h6">Working From Home</span>
                                <div class="custom-control custom-switch float-right">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch1" checked/>
                                    <label class="custom-control-label" for="customSwitch1"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex">
                    <div class="card settings-card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header w-100">
                            <h4 class="card-title mb-0 d-block">Focus Technologies</h4>
                        </div>
                        <div class="card-body">
                            <div id="company-name">
                                <div class="table-responsive bg-white">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Team Member</th>
                                                <th>Allowance</th>
                                                <th>Days Used</th>
                                                <th>Approvers</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><a href="/employment" class="text-primary"><span class="avatar" data-toggle="tooltip" data-placement="top" title="Maria Cotton"><img src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" alt="Maria Cotton" class="img-fluid"/></span><span>Maria Cotton</span></a></td>
                                                <td>25</td>
                                                <td>20</td>
                                                <td>Robert Wilson</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default TimeOff;