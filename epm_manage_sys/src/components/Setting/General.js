import React from 'react'

function General() {
    return (

        <div class="col-xl-9 col-lg-8 col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
                    <div class="card-body">
                        <ul class="list-group list-group-horizontal-lg">
                            <li class="list-group-item text-center active button-5"><a href="/general" class="text-white">General</a></li>
                            <li class="list-group-item text-center button-6"><a class="text-dark" href="/timeoff">Time Off</a></li>
                        </ul>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm company-logo flex-fill grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Company Logo</h4>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="row">
                                    <div class="col-12">
                                        <div class="avatar-upload">
                                            <div class="avatar-edit">
                                                <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                                <label for="imageUpload"></label>
                                            </div>
                                            <div>
                                                <img src="https://www.course.lk/uploads/institute/35/bcas-campus.jpg" alt="logo image" class="img-fluid" width="100" style={{height:'200px', width:'400px'}}/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 d-flex">
                    <div class="card ctm-border-radius shadow-sm flex-fill grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">
                                Your Company
                            </h4>
                        </div>
                        <div class="card-body">
                            <form>
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label>Company Url</label>
                                    <input type="text" class="form-control"/>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:void(0)" class="btn btn-theme button-1 ctm-border-radius text-white">Save Changes</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default General;