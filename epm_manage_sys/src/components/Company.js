import React from 'react'

function Company() {
    return (

    <div class="col-xl-9 col-lg-8 col-md-12">
                
        <div class="row">
            <div class="col-md-7 d-flex">
                <div class="card ctm-border-radius shadow-sm grow flex-fill">
                    <div class="card-header">
                        <h4 class="card-title mb-0">
                            Focus Technologies
                            <a href="javascript:void(0)" class="float-right text-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <p><span class="text-primary">Register Number : </span>FT0070</p>
                                <p><span class="text-primary">Incorporation Date : </span>07 May 2000</p>
                                <p><span class="text-primary">VAT Number : </span>VT0070</p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                    <span class="text-primary">Address:</span><br/>
                                    
                                    Santiago de Surco
                                    <br/> Av.Caminos del Inca 1325<br/>
                                    United States
                                </p>
                                
                            </div>
                        </div>
                        <div class="text-center mt-3">
                            <button class="btn btn-theme text-white ctm-border-radius button-1" data-toggle="modal" data-target="#add-information">Add Company Information</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 d-flex">
                <div class="card ctm-border-radius shadow-sm grow flex-fill">
                    <div class="card-header">
                        <h4 class="card-title mb-0">
                            Contact
                        </h4>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Contact" value="07448503267"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="focustechnology.com"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-0">
                            <input type="email" class="form-control" placeholder="hr@focustechnology.com"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="company-doc">
                    <div class="card ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title d-inline-block mb-0">
                                Documents
                            </h4>
                            <a href="javascript:void(0)" class="float-right add-doc text-primary" data-toggle="modal" data-target="#addDocument">Add Document
                                </a>
                        </div>
                        <div class="card-body">
                            <div class="employee-office-table">
                                <div class="table-responsive">
                                    <table class="table custom-table">
                                        <thead>
                                            <tr>
                                                <th>Type</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Size</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-primary"><i class="fa fa-file-pdf-o" data-toggle="tooltip" data-placement="top" title="PDF" aria-hidden="true"></i></td>
                                                <td><a href="javascript:void(0)" class="text-primary">Leave & Attendance Policy</a></td>
                                                <td>05 Jan 2019</td>
                                                <td>20 MB</td>
                                                <td class="text-right text-danger">
                                                    <div class="table-action">
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                            <span class="lnr lnr-trash"></span> Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-primary"><i class="fa fa-file-pdf-o" data-toggle="tooltip" data-placement="top" title="PDF" aria-hidden="true"></i></td>
                                                <td><a href="javascript:void(0)" class="text-primary">Dress Code Policy</a></td>
                                                <td>10 May 2019</td>
                                                <td>30 MB</td>
                                                <td class="text-right text-danger">
                                                    <div class="table-action">
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                            <span class="lnr lnr-trash"></span> Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-primary cursor-pointer"><i class="fa fa-file-word-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Word"></i></td>
                                                <td><a href="javascript:void(0)" class="text-primary">ID Card Policy</a></td>
                                                <td>12 Jun 2019</td>
                                                <td>25 MB</td>
                                                <td class="text-right text-danger">
                                                    <div class="table-action">
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                            <span class="lnr lnr-trash"></span> Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-primary"><i class="fa fa-file-pdf-o" data-toggle="tooltip" data-placement="top" title="PDF" aria-hidden="true"></i></td>
                                                <td><a href="javascript:void(0)" class="text-primary">Work From Home Policy</a></td>
                                                <td>05 Jul 2019</td>
                                                <td>10 MB</td>
                                                <td class="text-right text-danger">
                                                    <div class="table-action">
                                                        <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                            <span class="lnr lnr-trash"></span> Delete
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="card ctm-border-radius shadow-sm grow flex-fill">
                    <div class="card-header">
                        <div class="d-inline-block">
                            <h4 class="card-title mb-0">Focus Technologies</h4>
                            <p class="mb-0 ctm-text-sm">Head Office</p>
                        </div>
                        <div class="d-inline-block float-right" data-toggle="modal" data-target="#editOffice">
                            <a href="javascript:void(0)" class="btn btn-theme mt-2 text-white float-right ctm-border-radius" data-toggle="tooltip" data-placement="bottom" title="Edit"><i class="fa fa-pencil"></i>	</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4 class="card-title">Members</h4>
                        <a href="/employment"><span class="avatar" data-toggle="tooltip" data-placement="top" title="Danny Ward"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" class="img-fluid"/></span></a>
                        <a href="/employment"><span class="avatar" data-toggle="tooltip" data-placement="top" title="Linda Craver"><img class="img-fluid" alt="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/avatar image" src="assets/img/profiles/img-4.jpg"/></span></a>
                        <a href="/employment"><span class="avatar" data-toggle="tooltip" data-placement="top" title="Jenni Sims"><img class="img-fluid" alt="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/avatar image" src="assets/img/profiles/img-3.jpg"/></span></a>
                        <a href="/employment"><span class="avatar" data-toggle="tooltip" data-placement="top" title="Maria Cotton"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" class="img-fluid"/></span></a>
                        <a href="/employment"><span class="avatar" data-toggle="tooltip" data-placement="top" title="John Gibbs"><img class="img-fluid" alt="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/avatar image" src="assets/img/profiles/img-2.jpg"/></span></a>
                        <a href="employment"><span class="avatar" data-toggle="tooltip" data-placement="top" title="Richard Wilson"><img class="img-fluid" alt="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/avatar image" src="assets/img/profiles/img-10.jpg"/></span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 d-flex">
                <div class="card shadow-sm grow ctm-border-radius flex-fill">
                <div class="card-header">
                    <h4 class="card-title mb-0 d-inline-block">Overview </h4>
                    <a href="/teams" class="float-right text-primary">Manage Teams</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 col-6 text-center">
                            <h5 class="btn btn-outline-primary mt-3"><b>6</b></h5>
                            <p class="mb-3">Teams</p>
                        </div>
                        <div class="col-md-6 col-6 text-center">
                            <h5 class="btn btn-outline-primary mt-3"><b>7</b></h5>
                            <p class="mb-3">People</p>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="/all" class="btn btn-theme text-white ctm-border-radius mt-2 button-1">People Directory</a>
                    </div>
                </div>
            </div>
            
            </div>
        </div>
    </div>

    );
}

export default Company;