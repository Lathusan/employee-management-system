import React from 'react'

function Detail() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white p-4 mb-4 card">
            <ul class="list-group list-group-horizontal-lg">
                        <li class="list-group-item text-center active button-5"><a href="/employment" class="text-white">Employement</a></li>
                        <li class="list-group-item text-center button-6"><a href="/detail" class="text-dark">Detail</a></li>
                        <li class="list-group-item text-center button-6"><a href="/document" class="text-dark">Document</a></li>
                        <li class="list-group-item text-center button-6"><a href="/payroll" class="text-dark">Payroll</a></li>
                        <li class="list-group-item text-center button-6"><a href="/profiletimeoff" class="text-dark">Timeoff</a></li>
                        <li class="list-group-item text-center button-6"><a href="/profilereviews" class="text-dark">Reviews</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/settings">Settings</a></li>
                    </ul>
                </div>
            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-6 d-flex">
                    <div class="card flex-fill ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Basic Information</h4>
                        </div>
                        <div class="card-body text-center">
                            <p class="card-text mb-3"><span class="text-primary">Preferred Name :</span><b>	 Maria</b></p>
                            <p class="card-text mb-3"><span class="text-primary">First Name :</span> Maria</p>
                            <p class="card-text mb-3"><span class="text-primary">Last Name : </span>Cotton</p>
                            <p class="card-text mb-3"><span class="text-primary">Nationality :</span> American</p>
                            <p class="card-text mb-3"><span class="text-primary">Date of Birth :</span> 05 May 1990</p>
                            <p class="card-text mb-3"><span class="text-primary">Gender : </span>Female</p>
                            <p class="card-text mb-3"><span class="text-primary">Blood Group :</span> A+</p>
                            <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#add_basicInformation"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#edit_basicInformation"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 d-flex">
                    <div class="card flex-fill  ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Contact</h4>
                        </div>
                        <div class="card-body text-center">
                            <p class="card-text mb-3"><span class="text-primary">Phone Number : </span>987654321</p>
                            <p class="card-text mb-3"><span class="text-primary">Personal Email : </span>mariacotton@example.com</p>
                            <p class="card-text mb-3"><span class="text-primary">Secondary Number : </span>986754231</p>
                            <p class="card-text mb-3"><span class="text-primary">Web Site : </span>www.focustechnology.com</p>
                            <p class="card-text mb-3"><span class="text-primary">Linkedin : </span>#mariacotton</p>
                            <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#add_Contact"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#edit_Contact"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12 col-md-12">
                    <div class="row">
                        <div class="col-xl-12 col-lg-6 col-md-6 d-flex">
                            <div class="card ctm-border-radius shadow-sm grow flex-fill">
                                <div class="card-header">
                                    <h4 class="card-title mb-0">Dates</h4>
                                </div>
                                <div class="card-body text-center">
                                    <p class="card-text mb-3"><span class="text-primary">Start Date : </span>06 Jun 2017</p>
                                    <p class="card-text mb-3"><span class="text-primary">Visa Expiry Date : </span>06 Jun 2020</p>
                                    <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#edit_Dates"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-6 col-md-6 d-flex">
                            <div class="card ctm-border-radius shadow-sm grow flex-fill">
                                <div class="card-header">
                                    <h4 class="card-title d-inline-block mb-0">Dates</h4>
                                    <span class="float-right"><a href="javascript:void(0)" class="text-primary" data-toggle="modal" data-target="#addNewType"> New Type</a></span>
                                </div>
                                <div class="card-body">
                                    <div class="input-group mb-3">
                                        <input class="form-control datetimepicker date-enter" type="text" placeholder="Add Start Date"/>
                                        <div class="input-group-append">
                                            <button class="btn btn-theme text-white" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input class="form-control datetimepicker date-enter" type="text" placeholder="Add Visa Expiry Date"/>
                                        <div class="input-group-append">
                                            <button class="btn btn-theme text-white" type="button"><i class="fa fa-calendar" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white button-1">Add A Key Date</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Detail;