import React from 'react'

function Payroll() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white p-4 mb-4 card">
                <ul class="list-group list-group-horizontal-lg">
                    <li class="list-group-item text-center active button-5"><a href="/employment" class="text-white">Employement</a></li>
                    <li class="list-group-item text-center button-6"><a href="/detail" class="text-dark">Detail</a></li>
                    <li class="list-group-item text-center button-6"><a href="/document" class="text-dark">Document</a></li>
                    <li class="list-group-item text-center button-6"><a href="/payroll" class="text-dark">Payroll</a></li>
                    <li class="list-group-item text-center button-6"><a href="/profiletimeoff" class="text-dark">Timeoff</a></li>
                    <li class="list-group-item text-center button-6"><a href="/profilereviews" class="text-dark">Reviews</a></li>
                    <li class="list-group-item text-center button-6"><a class="text-dark" href="/settings">Settings</a></li>
                </ul>
            </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 d-flex">
                <div class="card flex-fill ctm-border-radius shadow-sm grow">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Payroll Details</h4>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Add Bank Name"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme ctm-border-radius text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Add Bank Account Number"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme ctm-border-radius text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Add Bank Sort Code"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme ctm-border-radius text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 d-flex">
                <div class="card flex-fill ctm-border-radius shadow-sm grow">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Payroll Details</h4>
                    </div>
                    <div class="card-body text-center">
                        <p class="card-text mb-3"><span class="text-primary">Bank Name : </span>Life Essence Banks, Inc.</p>
                        <p class="card-text mb-3"><span class="text-primary">Bank Account Number : </span>112300987652</p>
                        <p class="card-text mb-3"><span class="text-primary">Bank Sort Code : </span>LE00652</p>
                        <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#add_payroll"><i class="fa fa-plus" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white btn-sm" data-toggle="modal" data-target="#edit_payroll"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-12 d-flex">
                <div class="upload-doc mb-0 flex-fill">
                    <div class="card ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">P45</h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="document-up">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#upload-document"><i class="mr-2 text-primary fa fa-file-pdf-o" aria-hidden="true"></i>Upload P45 <span class="float-right text-primary">Edit</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12 d-flex">
                <div class="card ctm-border-radius shadow-sm grow flex-fill office-card-last">
                    <div class="card-header">
                        <h4 class="card-title mb-0">Salary</h4>
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-0">
                            <input type="text" class="form-control" placeholder="Current Salary"/>
                            <div class="input-group-append">
                                <button class="btn btn-theme text-white" type="button"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    );
}

export default Payroll;