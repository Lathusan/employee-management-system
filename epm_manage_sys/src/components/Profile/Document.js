import React from 'react'

function Document() {
    return (

        <div class="col-xl-9 col-lg-8 col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white p-4 mb-4 card">
            <ul class="list-group list-group-horizontal-lg">
                        <li class="list-group-item text-center active button-5"><a href="/employment" class="text-white">Employement</a></li>
                        <li class="list-group-item text-center button-6"><a href="/detail" class="text-dark">Detail</a></li>
                        <li class="list-group-item text-center button-6"><a href="/document" class="text-dark">Document</a></li>
                        <li class="list-group-item text-center button-6"><a href="/payroll" class="text-dark">Payroll</a></li>
                        <li class="list-group-item text-center button-6"><a href="/profiletimeoff" class="text-dark">Timeoff</a></li>
                        <li class="list-group-item text-center button-6"><a href="/profilereviews" class="text-dark">Reviews</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/settings">Settings</a></li>
                    </ul>
                </div>
            <div class="card ctm-border-radius shadow-sm grow">
                <div class="card-header">
                    <h4 class="card-title doc d-inline-block mb-0">Documents</h4>
                    <a href="javascript:void(0)" class="float-right doc-fold text-primary d-inline-block text-info" data-toggle="modal" data-target="#add-document">Add Document</a>
                </div>
                <div class="card-body doc-boby">
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title text-primary mb-0">Passport</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="document-up">
                                        <a href="javascript:void(0)"><i class="mr-2 text-primary fa fa-file-pdf-o" aria-hidden="true"></i> Passport.pdf <span class="float-right text-primary" data-toggle="modal" data-target="#upload-document">Edit</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow-none">
                        <div class="card-header">
                            <h5 class="card-title text-primary mb-0">P45</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="document-up">
                                        <a href="javascript:void(0)"><i class="mr-2 text-primary fa fa-file-pdf-o" aria-hidden="true"></i> P45.pdf<span class="float-right text-primary" data-toggle="modal" data-target="#upload-p45">Edit</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow-none ctm-margin-btm">
                        <div class="card-header">
                            <h5 class=" text-primary card-title mb-0">Visa</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="document-up">
                                        <a href="javascript:void(0)"><i class="mr-2 text-primary fa fa-file-pdf-o" aria-hidden="true"></i> Visa.pdf<span  data-toggle="modal" data-target="#upload-visa" class="float-right text-primary">Edit</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="add-doc text-center">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#add-document" class="btn btn-theme button-1 ctm-border-radius text-white text-center">Add New Document</a>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default Document;