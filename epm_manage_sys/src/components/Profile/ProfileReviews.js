import React from 'react'

function ProfileReviews() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white p-4 mb-4 card">
                <ul class="list-group list-group-horizontal-lg">
                    <li class="list-group-item text-center active button-5"><a href="/employment" class="text-white">Employement</a></li>
                    <li class="list-group-item text-center button-6"><a href="/detail" class="text-dark">Detail</a></li>
                    <li class="list-group-item text-center button-6"><a href="/document" class="text-dark">Document</a></li>
                    <li class="list-group-item text-center button-6"><a href="/payroll" class="text-dark">Payroll</a></li>
                    <li class="list-group-item text-center button-6"><a href="/profiletimeoff" class="text-dark">Timeoff</a></li>
                    <li class="list-group-item text-center button-6"><a href="/profilereviews" class="text-dark">Reviews</a></li>
                    <li class="list-group-item text-center button-6"><a class="text-dark" href="/settings">Settings</a></li>
                </ul>
                </div>
            <div class="card shadow-sm ctm-border-radius grow">
                <div class="card-header">
                    <h4 class="card-title mb-0">Review Forms</h4>
                </div>
                <div class="card-body">
                    <div class="employee-office-table">
                        <div class="table-responsive">
                        <table class="table custom-table table-hover">
                            <thead>
                                <tr>
                                    <th>Review Name</th>
                                    <th>Reviewers</th>
                                    <th>Begin On</th>
                                    <th>Due By</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Monthly Review</td>
                                    <td>
                                        <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                        <h2><a href="/employment"> Richard Wilson</a></h2>
                                    </td>
                                    <td>15 Dec 2019</td>
                                    <td>17 Dec 2019</td>
                                    <td>
                                        <div class="dropdown action-label drop-active">
                                            <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
                                                <a class="dropdown-item" href="javascript:void(0)"> Completed</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-action">
                                            <a href="edit-review.html" class="btn btn-sm btn-outline-success">
                                                <span class="lnr lnr-pencil"></span> Edit
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                <span class="lnr lnr-trash"></span> Delete
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Employees Review</td>
                                    <td>
                                        <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                        <h2><a href="/employment"> Richard Wilson</a></h2>
                                    </td>
                                    <td>15 Dec 2019</td>
                                    <td>17 Dec 2019</td>
                                    <td>
                                        <div class="dropdown action-label drop-active">
                                            <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
                                                <a class="dropdown-item" href="javascript:void(0)"> Completed</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-action">
                                            <a href="edit-review.html" class="btn btn-sm btn-outline-success">
                                                <span class="lnr lnr-pencil"></span> Edit
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                <span class="lnr lnr-trash"></span> Delete
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Employees Review</td>
                                    <td>
                                        <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                        <h2><a href="/employment"> Richard Wilson</a></h2>
                                    </td>
                                    <td>15 Dec 2019</td>
                                    <td>17 Dec 2019</td>
                                    <td>
                                        <div class="dropdown action-label drop-active">
                                            <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
                                                <a class="dropdown-item" href="javascript:void(0)"> Completed</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-action">
                                            <a href="edit-review.html" class="btn btn-sm btn-outline-success">
                                                <span class="lnr lnr-pencil"></span> Edit
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                <span class="lnr lnr-trash"></span> Delete
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Employees Review</td>
                                    <td>
                                        <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                        <h2><a href="/employment"> Richard Wilson</a></h2>
                                    </td>
                                    <td>15 Dec 2019</td>
                                    <td>17 Dec 2019</td>
                                    <td>
                                        <div class="dropdown action-label drop-active">
                                            <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
                                                <a class="dropdown-item" href="javascript:void(0)"> Completed</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-action">
                                            <a href="#" class="btn btn-sm btn-outline-success">
                                                <span class="lnr lnr-pencil"></span> Edit
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                <span class="lnr lnr-trash"></span> Delete
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Employees Review</td>
                                    <td>
                                        <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                        <h2><a href="/employment"> Richard Wilson</a></h2>
                                    </td>
                                    <td>15 Dec 2019</td>
                                    <td>17 Dec 2019</td>
                                    <td>
                                        <div class="dropdown action-label drop-active">
                                            <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> In Progress <i class="caret"></i></a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="javascript:void(0)"> In Progress</a>
                                                <a class="dropdown-item" href="javascript:void(0)"> Completed</a>
                                                
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="table-action">
                                            <a href="edit-review.html" class="btn btn-sm btn-outline-success">
                                                <span class="lnr lnr-pencil"></span> Edit
                                            </a>
                                            <a href="javascript:void(0);" class="btn btn-sm btn-outline-danger" data-toggle="modal" data-target="#delete">
                                                <span class="lnr lnr-trash"></span> Delete
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default ProfileReviews;