import React from 'react'

function ProfileTimeoff() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white p-4 mb-4 card">
                <ul class="list-group list-group-horizontal-lg">
                    <li class="list-group-item text-center active button-5"><a href="/employment" class="text-white">Employement</a></li>
                    <li class="list-group-item text-center button-6"><a href="/detail" class="text-dark">Detail</a></li>
                    <li class="list-group-item text-center button-6"><a href="/document" class="text-dark">Document</a></li>
                    <li class="list-group-item text-center button-6"><a href="/payroll" class="text-dark">Payroll</a></li>
                    <li class="list-group-item text-center button-6"><a href="/profiletimeoff" class="text-dark">Timeoff</a></li>
                    <li class="list-group-item text-center button-6"><a href="/profilereviews" class="text-dark">Reviews</a></li>
                    <li class="list-group-item text-center button-6"><a class="text-dark" href="/settings">Settings</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-xl-6 col-lg-12 col-md-6 col-12 d-flex">
                    <div class="card ctm-border-radius shadow-sm grow flex-fill">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Holidays List </h4>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7 col-sm-6">
                                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active history-btn" data-toggle="tab" href="#tabs-1" role="tab">Upcoming</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link history-btn" data-toggle="tab" href="#tabs-2" role="tab">History</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-5 col-sm-6">
                                    <div class="form-row form-group mt-3 mt-lg-0 mt-sm-0">
                                        <div class="col-sm-12">
                                            <select class="form-control select">
                                                <option selected>Apply Leave</option>
                                                <option>Working From Home</option>
                                                <option>Sick Leave</option>
                                                <option>Parental Leave</option>
                                                <option>Annual Leave</option>
                                                <option>Normal Leave</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="tab-content py-0" id="pills-tabContent">
                                        <div class="tab-pane py-0 active" id="tabs-1" role="tabpanel">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Date</th>
                                                            <th>Leave Reason</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Mon, 26 Aug 2019</td>
                                                            <td>Bank Holiday</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Wed, 25 Dec 2019</td>
                                                            <td>Christmas Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Thu, 26 Dec 2019</td>
                                                            <td>Bank Holiday</td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Wed, 1 Jan 2020</td>
                                                            <td>Bank Holiday</td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>Mon, 6 Jan 2020</td>
                                                            <td>Epiphany</td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td>Fri, 17 Jan 2020</td>
                                                            <td>Lee-Jackson Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>Mon, 9 Mar 2020</td>
                                                            <td>Presidents' Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>8</td>
                                                            <td>Wed, 1 Mar 2020</td>
                                                            <td>Holi</td>
                                                        </tr>
                                                        <tr>
                                                            <td>9</td>
                                                            <td>Sat, 4 Jul 2020</td>
                                                            <td>Independence Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>10</td>
                                                            <td>Mon, 7 Sep 2020</td>
                                                            <td>Labor Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>11</td>
                                                            <td>Mon, 11 Nov 2020</td>
                                                            <td>Veterans Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>12</td>
                                                            <td>Fri, 25 Dec 2020</td>
                                                            <td>Christmas Day</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane py-0" id="tabs-2" role="tabpanel">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Date</th>
                                                            <th>Leave Reason</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Tue, 1 Jan 2019</td>
                                                            <td>New Year's Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Mon, 21 Jan 2019</td>
                                                            <td>Martin Luther King Jr. Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>3</td>
                                                            <td>Mon, 18 Feb 2019</td>
                                                            <td>Presidents' Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>4</td>
                                                            <td>Mon, 14 Oct 2019</td>
                                                            <td>Columbus Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>5</td>
                                                            <td>Mon, 11 Nov 2019</td>
                                                            <td>Veterans Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>6</td>
                                                            <td>Fri, 17 Jan 2019</td>
                                                            <td>Lee-Jackson Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>7</td>
                                                            <td>Mon, 18 Feb 2019</td>
                                                            <td>Presidents' Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>8</td>
                                                            <td>Thu, 21 Mar 2019</td>
                                                            <td>Holi</td>
                                                        </tr>
                                                        <tr>
                                                            <td>9</td>
                                                            <td>Thu, 4 Jul 2019</td>
                                                            <td>Independence Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>11</td>
                                                            <td>Mon, 11 Nov 2019</td>
                                                            <td>Veterans Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>10</td>
                                                            <td>Thu, 28 Nov 2019</td>
                                                            <td>Thanksgiving Day</td>
                                                        </tr>
                                                        <tr>
                                                            <td>12</td>
                                                            <td>Wed, 25 Dec 2019</td>
                                                            <td>Christmas Day</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12 col-md-6 col-12 d-flex">
                    <div class="card flex-fill ctm-border-radius shadow-sm grow">
                        <div class="card-header">
                            <h4 class="card-title mb-0">Leave Off Details</h4>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Year</h5>
                            <p class="mb-0">01 January – 31 December</p>
                            <hr/>
                            <h5 class="card-title">Days Used</h5>
                            <div class="progress">
                                <div class="progress-bar bg-primary w-25"> 5 days</div>
                            </div>
                            <hr/>
                            <h5 class="card-title">Days</h5>
                            <p>5 Used</p>
                            <hr/>
                            <h5 class="card-title">Non Deductible Days</h5>
                            <p class="text-theme btn btn-theme ctm-border-radius btn-sm text-white d-inline-block">5 Approved</p>
                            <p class="text-theme btn btn-theme ctm-border-radius btn-sm text-white d-inline-block">7 Pending</p>
                            <hr/>
                            <h5 class="card-title">Attendance</h5>
                            <p class="mb-3"><img class="mr-2 leave-img img-fluid" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/plaster.svg" alt="Sick Leave"/>3 Sick Days</p>
                            <p class="mb-0"><img class="mr-2 leave-img img-fluid" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/desk-lamp.svg" alt="Working from Home"/>2 Working from Home</p>
                            <hr/>
                            <h4 class="card-title d-inline-block">Time Off Approvers</h4>
                            <div class="form-row form-group mb-0">
                                <div class="col-sm-12">
                                    <select class="form-control select">
                                        <option selected>Choose Approver</option>
                                        <option>Admin</option>
                                        <option>Richard Wilson</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default ProfileTimeoff;