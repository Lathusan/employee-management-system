import React from 'react'

function TeamReports() {
    return (

    <div class="col-xl-9 col-lg-8  col-md-12">
        <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
                <div class="card-body">
                    <ul class="list-group list-group-horizontal-lg">
                        <li class="list-group-item text-center active button-5"><a href="/team-reports" class="text-white">Team Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/leave-reports">Leave Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/payroll-reports">Payroll Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/contact-reports">Contact Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/email-reports">Email Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/security-reports">Security Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/working-from-home-reports">Working From Home Reports</a></li>
                    </ul>
                </div>
            </div>
            <div class="card shadow-sm ctm-border-radius grow">
                <div class="card-body align-center">
                    <div class="row filter-row">
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3"> 
                            <div class="form-group mb-xl-0 mb-md-2 mb-sm-2">
                                <select class="form-control select">
                                    <option selected>Start Date</option>
                                    <option>Date Of Birth</option>
                                    <option>Created At</option>
                                    <option>Leaving Date</option>
                                    <option>Visa Expiry Date</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">  
                            <div class="form-group mb-lg-0 mb-md-2 mb-sm-2">
                                <input type="text" class="form-control datetimepicker" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">  
                            <div class="form-group mb-lg-0 mb-md-0 mb-sm-0">
                                <input type="text" class="form-control datetimepicker" placeholder="To"/>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">  
                            <a href="#" class="btn btn-theme button-1 text-white btn-block p-2 mb-md-0 mb-sm-0 mb-lg-0 mb-0"> Apply Filter </a>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm grow ctm-border-radius">
                <div class="card-body align-center">
                    <ul class="nav flex-row nav-pills" id="pills-tab" role="tablist" >
                        <li class="nav-item mr-2">
                            <a class="nav-link active mb-2" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Official Reports</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Personal reports</a>
                        </li>
                    </ul>
                </div>
            </div>
        <div class="card shadow-sm grow ctm-border-radius">
            <div class="card-body align-center">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="employee-office-table">
                            <div class="table-responsive">
                                <table class="table custom-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Active</th>
                                            <th>Employment</th>
                                            <th>Email</th>
                                            <th>Job title</th>
                                            <th>Line Manager</th>
                                            <th>Team name</th>
                                            <th>Start Date</th>
                                            <th>Company Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" class="img-fluid"/></a>
                                                <h2><a href="/employment">Danny Ward</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>danyward@example.com</td>
                                            <td>Team Lead</td>
                                            <td>Richard Wilson</td>
                                            <td>Designing</td>
                                            <td>05 Jan 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-4.jpg"/></a>
                                                <h2><a href="/employment"> Linda Craver</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>lindacraver@example.com</td>
                                            <td>Team Lead</td>
                                            <td>Richard Wilson</td>
                                            <td>IOS</td>
                                            <td>07 Jun 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-3.jpg"/></a>
                                                <h2><a href="/employment">Jenni Sims</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>jennisims@example.com</td>
                                            <td>Team Lead</td>
                                            <td>Richard Wilson</td>
                                            <td>Android</td>
                                            <td>05 Apr 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" class="img-fluid"/></a>
                                                <h2><a href="/employment"> Maria Cotton</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>mariacotton@example.com</td>
                                            <td>Team Lead</td>
                                            <td>Richard Wilson</td>
                                            <td>PHP </td>
                                            <td>05 Jan 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-2.jpg"/></a>
                                                <h2><a href="/employment"> John Gibbs</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>johndrysdale@example.com</td>
                                            <td>Team Lead</td>
                                            <td>Richard Wilson</td>
                                            <td>Business</td>
                                            <td>12 Feb 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                                <h2><a href="/employment"> Richard Wilson</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>richardwilson@example.com</td>
                                            <td>Team Lead</td>
                                            <td>No</td>
                                            <td>Admin</td>
                                            <td>05 Jan 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-8.jpg"/></a>
                                                <h2><a href="/employment"> Stacey Linville</a></h2>
                                            </td>
                                            <td>
                                                <div class="dropdown action-label drop-active">
                                                    <a href="javascript:void(0)" class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown"> Active <i class="caret"></i></a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="javascript:void(0)"> Active</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Inactive</a>
                                                        <a class="dropdown-item" href="javascript:void(0)"> Invited</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>Permanent</td>
                                            <td>staceylinville@example.com</td>
                                            <td>Team Lead</td>
                                            <td>Richard Wilson</td>
                                            <td>Testing</td>
                                            <td>05 Jan 2019</td>
                                            <td>Focus Technologies</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="employee-office-table">
                            <div class="table-responsive">
                                <table class="table custom-table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Salary Current</th>
                                            <th>Date Of Birth</th>
                                            <th>Phone Number</th>
                                            <th>Address</th>
                                            <th>Bank Name</th>
                                            <th>Account Number</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" class="img-fluid"/></a>
                                                <h2><a href="/employment">Danny Ward</a></h2>
                                            </td>
                                            <td>
                                                Male
                                            </td>
                                            <td>$3000</td>
                                            <td>25 Jun 1984</td>
                                            <td>9876543231</td>
                                            <td>201 Lunetta Street,Plant City,<br/> Florida(FL), 33566</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987652</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-4.jpg"/></a>
                                                <h2><a href="/employment"> Linda Craver</a></h2>
                                            </td>
                                            <td>
                                                Female
                                            </td>
                                            <td>$2000</td>
                                            <td>14 Feb 1984</td>
                                            <td>9876543221</td>
                                            <td>683 Longview Avenue,New York, <br/> New York(NY), 10011</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987662</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-3.jpg"/></a>
                                                <h2><a href="/employment">Jenni Sims</a></h2>
                                            </td>
                                            <td>
                                                Female
                                            </td>
                                            <td>$4000</td>
                                            <td>20 Jan 1984</td>
                                            <td>9876534214</td>
                                            <td> 4923 Front Street,Detroit,<br/> Michigan(MI), 48226</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987653</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" class="img-fluid"/></a>
                                                <h2><a href="/employment"> Maria Cotton</a></h2>
                                            </td>
                                            <td>
                                                Female
                                            </td>
                                            <td>$5000</td>
                                            <td>15 Jul 1984</td>
                                            <td>9876541123</td>
                                            <td>1246 Parkway Street, Brawley, <br/>California(CA), 92227</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987654</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-2.jpg"/></a>
                                                <h2><a href="/employmentl"> John Gibbs</a></h2>
                                            </td>
                                            <td>
                                                Male
                                            </td>
                                            <td>$4500</td>
                                            <td>05 Dec 1984</td>
                                            <td>9876541132</td>
                                            <td>4604 Fairfax Drive,Rochelle Park,<br/> New Jersey(NJ), 07662</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987655</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                                <h2><a href="/employment"> Richard Wilson</a></h2>
                                            </td>
                                            <td>
                                                Male
                                            </td>
                                            <td>$4600</td>
                                            <td>25 Apr 1984</td>
                                            <td>9876541321</td>
                                            <td>3088 Gordon Street, Los Angeles,<br/> California(CA), 90017</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987656</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-8.jpg"/></a>
                                                <h2><a href="/employment">Stacey Linville</a></h2>
                                            </td>
                                            <td>
                                                Female
                                            </td>
                                            <td>$4700</td>
                                            <td>23 Jan 1984</td>
                                            <td>9876542312</td>
                                            <td>835 Sarah Drive,Lafayette,<br/> Louisiana(LA), 70506</td>
                                            <td>Life Essence Banks, Inc.</td>
                                            <td>112300987657</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center mt-3">
                    <a href="javascript:void(0)" class="btn btn-theme button-1 ctm-border-radius text-white">Download Report</a>
                </div>
            </div>
        </div>
    </div>

    );
}

export default TeamReports;