import React from 'react'

function ContactReports() {
    return (

        <div class="col-xl-9 col-lg-8  col-md-12">
            <div class="quicklink-sidebar-menu ctm-border-radius shadow-sm grow bg-white card">
                <div class="card-body">
                <ul class="list-group list-group-horizontal-lg">
                        <li class="list-group-item text-center active button-5"><a href="/team-reports" class="text-white">Team Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/leave-reports">Leave Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/payroll-reports">Payroll Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/contact-reports">Contact Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/email-reports">Email Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/security-reports">Security Reports</a></li>
                        <li class="list-group-item text-center button-6"><a class="text-dark" href="/working-from-home-reports">Working From Home Reports</a></li>
                    </ul>
                </div>
            </div>	
            <div class="card shadow-sm ctm-border-radius grow">
                <div class="card-body align-center">
                    <div class="row filter-row">
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3"> 
                            <div class="form-group mb-xl-0 mb-md-2 mb-sm-2">
                                <select class="form-control select">
                                    <option selected>Start Date</option>
                                    <option>Date Of Birth</option>
                                    <option>Created At</option>
                                    <option>Leaving Date</option>
                                    <option>Visa Expiry Date</option>
                                </select>
                                
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">  
                            <div class="form-group mb-lg-0 mb-md-2 mb-sm-2">
                                <input type="text" class="form-control datetimepicker" placeholder="From"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">  
                            <div class="form-group mb-lg-0 mb-md-0 mb-sm-0">
                                <input type="text" class="form-control datetimepicker" placeholder="To"/>
                            </div>
                        </div>
                        
                        <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3">  
                            <a href="#" class="btn btn-theme button-1 text-white btn-block p-2 mb-md-0 mb-sm-0 mb-lg-0 mb-0"> Apply Filter </a>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow-sm ctm-border-radius grow">
                <div class="card-body align-center">
                    <div class="employee-office-table">
                        <div class="table-responsive">
                            <table class="table custom-table table-hover">
                                <thead>
                                    <tr>
                                        <th>Team Member</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Secondary Phone Number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-5.jpg" class="img-fluid"/></a>
                                            <h2><a href="/employment">Danny Ward</a></h2>
                                        </td>
                                        
                                        <td>danyward@example.com</td>
                                        <td>9876543215</td>
                                        <td>9876543512</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-4.jpg"/></a>
                                            <h2><a href="/employment"> Linda Craver</a></h2>
                                        </td>
                                        
                                        <td>lindacraver@example.com</td>
                                        <td>7896543213</td>
                                        <td>7896543321</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-3.jpg"/></a>
                                            <h2><a href="/employment">Jenni Sims</a></h2>
                                        </td>
                                        
                                        <td>jennisims@example.com</td>
                                        <td>8976453211</td>
                                        <td>8976453299</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-6.jpg" class="img-fluid"/></a>
                                            <h2><a href="/employment"> Maria Cotton</a></h2>
                                        </td>
                                        
                                        <td>mariacotton@example.com</td>
                                        <td>7689234519</td>
                                        <td>7689235419</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-2.jpg"/></a>
                                            <h2><a href="/employment"> John Gibbs</a></h2>
                                        </td>
                                        
                                        <td>johndrysdale@example.com</td>
                                        <td>7836271912</td>
                                        <td>7836271192</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-10.jpg"/></a>
                                            <h2><a href="/employment"> Richard Wilson</a></h2>
                                        </td>
                                        
                                        <td>richardwilson@example.com</td>
                                        <td>6089745321</td>
                                        <td>6089745322</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="/employment" class="avatar"><img class="img-fluid" alt="avatar image" src="http://dreamguys.co.in/demo/dleohr/template-1/dleohr-vertical/assets/img/profiles/img-8.jpg"/></a>
                                            <h2><a href="/employment">Stacey Linville</a></h2>
                                        </td>
                                        
                                        <td>staceylinville@example.com</td>
                                        <td>8075643219</td>
                                        <td>8075643218</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <a href="javascript:void(0)" class="btn btn-theme ctm-border-radius text-white button-1">Download Report</a>
                    </div>
                </div>
            </div>
        </div>

    );
}

export default ContactReports;