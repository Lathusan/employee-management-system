import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route} from 'react-router-dom';
import Navigationbar from './components/Navigationbar';
import SideNav from './components/SideNav';
import Dashboard from './components/Dashboard';
import All from './components/Employee/All';
import Teams from './components/Employee/Teams';
import Office from './components/Employee/Office';
import AddPerson from './components/Employee/AddPerson';
import Company from './components/Company';
import Leave from './components/Leave';
import Reviews from './components/Reviews';
import TeamReports from './components/Reports/TeamReports';
import LeaveReports from './components/Reports/LeaveReports';
import PayrollReports from './components/Reports/PayrollReports';
import ContactReports from './components/Reports/ContactReports';
import EmailReports from './components/Reports/EmailReports';
import SecurityReports from './components/Reports/SecurityReports';
import WorkingFromHomeReports from './components/Reports/WorkingFromHomeReports';
import AccountRoles from './components/Manage/AccountRoles';
import LeadershipRoles from './components/Manage/LeadershipRoles';
import General from './components/Setting/General';
import TimeOff from './components/Setting/TimeOff';
import Employment from './components/Profile/Employment';
import Detail from './components/Profile/Detail';
import Document from './components/Profile/Document';
import Payroll from './components/Profile/Payroll';
import ProfileTimeoff from './components/Profile/ProfileTimeoff';
import ProfileReviews from './components/Profile/ProfileReviews';
import Settings from './components/Profile/Settings';

function App() {
  return (
    <div class="inner-wrapper">
      <div id="loader-wrapper">
				
				<div class="loader">
				  <div class="dot"></div>
				  <div class="dot"></div>
				  <div class="dot"></div>
				  <div class="dot"></div>
				  <div class="dot"></div>
				</div>

			</div>

      <header class="header">
     
      <Navigationbar/>

      </header>

      <div class="page-wrapper">
				<div class="container-fluid">
					<div class="row">

            <BrowserRouter>
              <SideNav/>

              <Route path='/dashboard' component={Dashboard}/>
              <Route path='/all' component={All}/>
              <Route path='/teams' component={Teams}/>
              <Route path='/office' component={Office}/>
              <Route path='/addperson' component={AddPerson}/>
              <Route path='/company' component={Company}/>
              <Route path='/leave' component={Leave}/>
              <Route path='/reviews' component={Reviews}/>
              <Route path='/team-reports' component={TeamReports}/>
              <Route path='/leave-reports' component={LeaveReports}/>
              <Route path='/payroll-reports' component={PayrollReports}/>
              <Route path='/contact-reports' component={ContactReports}/>
              <Route path='/email-reports' component={EmailReports}/>
              <Route path='/security-reports' component={SecurityReports}/>
              <Route path='/working-from-home-reports' component={WorkingFromHomeReports}/>
              <Route path='/account-roles' component={AccountRoles}/>
              <Route path='/leadership-roles' component={LeadershipRoles}/>
              <Route path='/general' component={General}/>
              <Route path='/timeoff' component={TimeOff}/>
              <Route path='/employment' component={Employment}/>
              <Route path='/detail' component={Detail}/>
              <Route path='/document' component={Document}/>
              <Route path='/payroll' component={Payroll}/>
              <Route path='/profiletimeoff' component={ProfileTimeoff}/>
              <Route path='/profilereviews' component={ProfileReviews}/>
              <Route path='/settings' component={Settings}/>

            </BrowserRouter>
  
          </div>
        </div>
      </div>

    </div>
  );
}

export default App;
